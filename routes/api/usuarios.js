var express = requiere('express');
var routes = express.Router();
var UsuarioController = requiere('../../controllers/api/usuarioControllerApi');

router.get('/', usuarioController.usuarios_list);
router.post('/create', usuarioController.usuarios_create);
router.post('/reservar', usuarioController.usuario_reservar);

module.exports = router;